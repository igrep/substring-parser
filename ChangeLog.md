# 0.4.1.0

- Add: Make some of the APIs compatible with parser libraries other than attoparsec.

# 0.4.0.0

- Add: `matchAll` function to make the given parser to collect all matching substrings.

# 0.3.0.0

- Add: `takeMatch` function to make the given parser match partially, by returning the prefixed string with parsed value

# 0.2.0.0

- Fix: forgot to specify exposed-modules.
- Fix: returns the source text if parser doesn't match.

# 0.1.0.0

- Initial release.
